package v1

import (
	"github.com/gin-gonic/gin"
)

func SetRoutes(router *gin.Engine) {
    versionGroup := router.Group("/v1")
    
    versionGroup.GET("/", func (c *gin.Context) {
        c.String(200, "Version 1")
    })
    
    // /documents
    documentsGroup := versionGroup.Group("/documents")
    documentsGroup.GET("/", getDocuments)
    documentsGroup.POST("/", createDocument)
    documentsGroup.GET("/:id", getDocument)
    documentsGroup.PUT("/:id", updateDocument)
    documentsGroup.DELETE("/:id", deleteDocument)
    documentsGroup.PUT("/:id/state", setDocumentState)
    
    // /sequences
    sequencesGroup := versionGroup.Group("/sequences")
    sequencesGroup.GET("/", getSequences)
    sequencesGroup.POST("/", createSequence)
    sequencesGroup.GET("/:id", getSequence)
    
    // /taxes
    taxesGroup := versionGroup.Group("/taxes")
    taxesGroup.GET("/", getTaxes)
    taxesGroup.POST("/", createTax)
    taxesGroup.GET("/:id", getTax)
    taxesGroup.PUT("/:id", updateTax)
    taxesGroup.DELETE("/:id", deleteTax)
}