package routes

import (
	"github.com/gin-gonic/gin"
    "codeberg.org/openinvoice/documents/routes/v1"
)

var (
	router = gin.Default()
)

func Run() {
    setRoutes()
    router.Run()
}

func setRoutes() {
    router.GET("/", func (c *gin.Context) {
        c.String(200, "")
    })
    v1.SetRoutes(router)
}