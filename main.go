package main

import (
    "codeberg.org/openinvoice/documents/routes"
)

func main() {
	routes.Run() // listen and serve on 0.0.0.0:8080
}
